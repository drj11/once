package main

import (
	"bufio"
	"flag"
	"hash/fnv"
	"io"
	"log"
	"os"
)

type Memory []uint64

func main() {
	flag.Parse()

	arg := flag.Args()
	if len(arg) == 0 {
		arg = []string{"-"}
	}

	out := bufio.NewWriter(os.Stdout)
	defer out.Flush()

	var memory Memory
	memory = make([]uint64, 1000000)

	for _, p := range arg {
		r, err := Open(p)
		if err != nil {
			log.Println(err)
			continue
		}

		inp := bufio.NewReader(r)

		err = Once(inp, out, memory)
		if err != nil {
			log.Println(err)
		}
	}

}

func Once(inp *bufio.Reader, out io.Writer, memory Memory) (err error) {
	for {
		line, err := inp.ReadBytes('\n')
		if len(line) > 0 {
			if memory.remember(line) {
				out.Write(line)
			}
		}
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}
	}
}

// Remember the string "line";
// returning true if not previously remembered.
// Memory is not reliable in that
// it may falsely report that things are previously remembered.
func (memory Memory) remember(line []byte) bool {
	h := fnv.New64a()
	h.Write(line)
	s := h.Sum64()

	// Width of word in bits
	W := uint64(64)

	// Number of bits in memory
	l := uint64(len(memory)) * W
	// Index, reduced to range [0, l)
	i := s % l
	b := ((memory[i/W] >> (i % W)) & 1)
	memory[i/W] |= (1 << (i % W))
	return b == 0
}

func Open(path string) (io.Reader, error) {
	if path == "-" {
		return os.Stdin, nil
	}
	return os.Open(path)
}
